﻿using System;
using UnityEngine;

namespace UI
{
    public class FinishPopup : MonoBehaviour
    {
        private Action _reset;
        public void Show(Action reset)
        {
            _reset = reset;
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Restart()
        {
            _reset?.Invoke();
            Hide();
        }
    }
}
