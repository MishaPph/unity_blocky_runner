using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class GameHud : MonoBehaviour
    {
        [SerializeField] private Text _score;

        public void SetScore(int score)
        {
            _score.text = score.ToString();
        }
    }
}
