using UnityEngine;

public class Collectable : MonoBehaviour
{
    [SerializeField] private ParticleSystem _collectFx;
    [SerializeField] private GameObject[] _bodies;
    
    private void OnEnable()
    {
        _collectFx.gameObject.SetActive(false);
        _collectFx.Clear();
        foreach (var bd in _bodies)
        {
            bd.SetActive(true);
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        _collectFx.gameObject.SetActive(true);
        _collectFx.Play(true);
        foreach (var bd in _bodies)
        {
            bd.SetActive(false);
        }
    }
}
