﻿using System;
using UnityEngine;
public class PlayerModel: MonoBehaviour
{
    [SerializeField] private TrailRenderer[] _trails;
    [SerializeField] private GameObject _shield;
    
    public void RepositionBy(float value)
    {
        foreach (var trail in _trails)
        {
            var mesh = new Vector3[trail.positionCount];
            trail.GetPositions(mesh);
            for (var j = 0; j < mesh.Length; j++)
            {
                mesh[j].z -= value;
            }
            trail.SetPositions(mesh);
        }
    }

    public void Reset()
    {
        foreach (var trail in _trails)
        {
            trail.SetPositions(new Vector3[]{});
            trail.Clear();
            var mesh = new Vector3[trail.positionCount];
            trail.GetPositions(mesh);
            for (var j = 0; j < mesh.Length; j++)
            {
                mesh[j] = transform.position;
            }
            trail.SetPositions(mesh);
        }
    }
    public bool ShieldVisible => _shield.activeSelf;
    
    public void ShowShield()
    {
        _shield.SetActive(true);   
    }

    public void HideShield()
    {
        _shield.SetActive(false);
    }
}
