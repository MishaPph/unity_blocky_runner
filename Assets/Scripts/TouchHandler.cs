using System;
using UnityEngine;

public class TouchHandler : MonoBehaviour
{
    public event Action SwipeLeft;
    public event Action SwipeRight;

    [SerializeField] private float _minSensitivity = 10;
    
    private Vector2 _pressPosition;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _pressPosition = Input.mousePosition;
        } else if (Input.GetMouseButtonUp(0))
        {
            var direct = (Vector2)Input.mousePosition - _pressPosition;
            if (Mathf.Abs(direct.x) < _minSensitivity)
                return;
            
            if (direct.x < 0)
            {
                SwipeLeft?.Invoke();
            }
            else
            {
                SwipeRight?.Invoke();
            }
        }
    }
}
