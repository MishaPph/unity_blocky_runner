using System;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour
{
    [SerializeField] private Generator[] _generators;
    [SerializeField] private Transform _target;
    [SerializeField] private float _outDistance = 10;
    [SerializeField] private float _visibleDistance = 30;
    [SerializeField] private int _startOffset = 10;
    
    private readonly Queue<Transform> _active = new Queue<Transform>();

    private int _currentPos;

    public event Action Reposition;
    private void Awake()
    {
        _currentPos = _startOffset;
        foreach (var generator in _generators)
        {
            generator.Init(_startOffset);
        }
    }
    private void GenerateNext()
    {
        _currentPos++;
        foreach (var generator in _generators)
        {
            if (!generator.Generate(_currentPos, out var row)) 
                continue;
            
            _active.Enqueue(Create(generator.Prefab, _currentPos, row));
            break;
        }

        if (_currentPos - _visibleDistance > Const.RepositionRate)
        {
            Reposition?.Invoke();
        }
    }

    private Transform Create(GameObject prefab, int pos, int row)
    {
        if (!PoolManager.Instance.TryGet(prefab.tag,out var item))
        {
            item = Instantiate(prefab, transform).transform;
        }
        item.localPosition = new Vector3(row * Const.RoadSize, 0, pos);
        return item;
    }
    
    private void FixedUpdate()
    {
        while ((_currentPos - _visibleDistance) < _target.localPosition.z)
        {
            _currentPos += 1;
            CullOut();
            GenerateNext();
        }
    }

    private void CullOut()
    {
        if (_active.Count <= 0) return;
        
        if (_active.Peek().localPosition.z + _outDistance < _target.localPosition.z)
        {
            PoolManager.Instance.Return(_active.Dequeue());
        }
    }
    
    public void Reset()
    {
        _currentPos = _startOffset;
        while (_active.Count > 0)
        {
            PoolManager.Instance.Return(_active.Dequeue());
        }
        foreach (var generator in _generators)
        {
            generator.Restart(_startOffset);
        }
    }
    public void RepositionBy(int reposition)
    {
        _currentPos -= reposition;
        foreach (var item in _active)
        {
            var pos= item.localPosition;
            pos.z -= reposition;
            item.localPosition = pos;
        }

        foreach (var gn in _generators)
        {
            gn.RepositionBy(reposition);
        }
    }
}
