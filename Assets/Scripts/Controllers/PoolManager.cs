﻿using System.Collections.Generic;
using UnityEngine;
public class PoolManager
{
    private static PoolManager _instance;
    public static PoolManager Instance => _instance ??= new PoolManager();

    private readonly Dictionary<string, Queue<Transform>> _pool = new Dictionary<string, Queue<Transform>>();
    
    public bool TryGet(string tag, out Transform item)
    {
        if (_pool.TryGetValue(tag, out var queue) && queue.Count > 0)
        {
            item = queue.Dequeue();
            item.gameObject.SetActive(true);
            return true;
        }
        item = null;
        return false;
    }
    
    public void Return(Transform item)
    {
        item.gameObject.SetActive(false);
        if (!_pool.TryGetValue(item.tag, out var queue))
        {
            _pool.Add(item.tag, new Queue<Transform>());
        }else
            queue.Enqueue(item);
    } 
    
}
