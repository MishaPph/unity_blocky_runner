﻿using System;
using UnityEngine;

namespace Controllers
{
    public class PlayerController: MonoBehaviour
    {
        [SerializeField] private TouchHandler _touchHandler;
    
        [SerializeField] private float _baseSpeed = 1;
        
        [SerializeField, Range(0, 0.01f)] private float _acceleration = 0.01f;
        
        [SerializeField] private PlayerModel _model;
    
        private int _indexPositions = 0;
        private Transform _cacheTransform;
        public bool Death { get; private set; }
    
        public event Action Died;

        public float Speed { get; private set; }
        public float Passed { get; private set; }
        private void Awake()
        {
            _cacheTransform = transform;
            _touchHandler.SwipeLeft += OnSwipeLeft;
            _touchHandler.SwipeRight += OnSwipeRight;
            
            Speed = _baseSpeed;
            _model.transform.SetParent(null);
            _model.HideShield();
        }

        private void OnSwipeLeft()
        {
            if(Death)
                return;
            _indexPositions = Mathf.Max(-Const.CountRows/2, _indexPositions - 1);
            MoveTo(_indexPositions);
        }
    
        private void OnSwipeRight()
        {
            if(Death)
                return;
            _indexPositions = Mathf.Min(Const.CountRows/2, _indexPositions + 1);
            MoveTo(_indexPositions);
        }

        private void MoveTo(int index)
        {
            var position = _cacheTransform.position;
            position.x = index * Const.RoadSize;
            _cacheTransform.position = position;
        }

        private void Update()
        {
            if(Death)
                return;

            Speed += _acceleration*_acceleration * Time.time;
            var add = Speed * Time.deltaTime;
            Passed += add;
            _cacheTransform.Translate(0, 0 ,add);
        }

        private void LateUpdate()
        {
            var t = _cacheTransform.localPosition;
            _model.transform.position = new Vector3(Mathf.Lerp(_model.transform.position.x, t.x, Time.deltaTime*10), t.y, t.z);
        }
        
        private void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag(TagHelper.Heal))
            {
                _model.ShowShield();
            } else  if(other.CompareTag(TagHelper.Obstacle))
            {
                if (_model.ShieldVisible)
                {
                    _model.HideShield();
                }
                else
                {
                    Death = true;
                    Died?.Invoke();
                }
            }
        }
    
        public void Reset()
        {
            Death = false;
            _indexPositions = 0;
            _cacheTransform.localPosition = Vector3.zero;
            
            _model.transform.position = Vector3.zero;
            _model.HideShield();
            _model.Reset();
            
            Speed = _baseSpeed;
            Passed = 0;
        }
        
        public void RepositionBy(float value)
        {
            var pos = _cacheTransform.localPosition;
            _cacheTransform.localPosition = new Vector3(pos.x, pos.y, pos.z - value);
            _model.transform.position = _cacheTransform.localPosition;
            _model.RepositionBy(value);
        }
    }
}