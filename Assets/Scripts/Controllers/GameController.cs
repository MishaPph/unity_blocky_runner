using TMPro;
using UI;
using UnityEngine;

namespace Controllers
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private PlayerController _player;
        [SerializeField] private ObstacleManager _obstacleManager;
        [SerializeField] private GroundManager _groundManager;
        [SerializeField] private CameraController _cameraController;
        
        [SerializeField] private FinishPopup _popup;
        [SerializeField] private GameHud _gameHud;
        
        private void Start()
        {
            _player.Died += OnPlayerDied;
            _obstacleManager.Reposition += OnReposition;
        }
        private void OnReposition()
        {
            _player.RepositionBy(Const.RepositionRate);
            _obstacleManager.RepositionBy(Const.RepositionRate);
            _groundManager.RepositionBy(Const.RepositionRate);
        }
        private void OnPlayerDied()
        {
            _cameraController.PlayShake();
            _popup.Show(Reset);
        }
    
        public void Reset()
        {
            _player.Reset();
            _obstacleManager.Reset();
            _groundManager.Reset();
        }
    
        private void Update()
        {
            _gameHud.SetScore((int)_player.Passed);
            
            if (Input.GetKeyDown(KeyCode.R))
            {
                Reset();
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                OnReposition();
            }
            
            if (Input.GetKeyDown(KeyCode.S))
            {
                _cameraController.PlayShake();
            }
        }
    }
}