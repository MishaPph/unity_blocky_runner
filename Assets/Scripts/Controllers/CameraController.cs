using UnityEngine;
using Random = UnityEngine.Random;
namespace Controllers
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private Transform _target;
        [SerializeField] private Vector3 _offset;

        private float shakeDuration;
        [SerializeField] private float shakeMagnitude = 2;

        private Vector3 _initialPosition;

        private void Awake()
        {
            _initialPosition = transform.position;
        }
        
        private void LateUpdate()
        {
            var pos = _initialPosition;
            pos.z = _target.position.z + _offset.z;
            transform.position = pos;
            
            if (shakeDuration > 0)
            {
                transform.position += Random.insideUnitSphere * (shakeMagnitude * shakeDuration);
                shakeDuration -= Time.deltaTime;
            }
            else
            {
                shakeDuration = 0f;
            }
        }

        public void PlayShake()
        {
            shakeDuration = 0.4f;
        }
    }
}
