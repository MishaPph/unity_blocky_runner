﻿using System.Collections.Generic;
using UnityEngine;

public class GroundManager : MonoBehaviour
{
    private const int CountActiveBlocks = 4;
    [SerializeField] private Transform _target;
    [SerializeField] private GameObject _prefab;
    [SerializeField] private float _blockSize = 15;
    [SerializeField] private float _cullDistance = 20;
    
    private readonly Queue<Transform> _active = new Queue<Transform>();

    private float _lastPos;

    private void Start()
    {
        GenerateBlocks();
    }

    private void GenerateBlocks()
    {
        for (var i = 0; i < CountActiveBlocks; i++)
        {
            Add(Create());
        }
    }
    
    private Transform Create()
    {
        return Instantiate(_prefab, transform).transform;
    }

    private void Update()
    {
        if (_active.Count <= 0) return;
        
        if (_active.Peek().position.z + _cullDistance < _target.position.z)
        {
            Add( _active.Dequeue());
        }
    }

    private void Add(Transform tr)
    {
        tr.localPosition = new Vector3(0, -0.5f, _lastPos);
        _active.Enqueue(tr);
        _lastPos += _blockSize;
    }

    public void Reset()
    {
        _lastPos = 0;
        for (var i = 0; i < _active.Count; i++)
        {
            Add( _active.Dequeue());
        }
    }
    public void RepositionBy(int reposition)
    {
        _lastPos -= reposition;
        foreach (var item in _active)
        {
           var pos = item.localPosition;
           pos.z -= reposition;
           item.localPosition = pos;
        }
    }
}
