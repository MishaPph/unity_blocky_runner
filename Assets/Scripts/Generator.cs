﻿using System;
using UnityEngine;

[Serializable]
public class Generator
{
    [SerializeField] private GameObject _prefab;

    private int _nextPosition;
    public GameObject Prefab => _prefab;
    [SerializeField] private Randomizer _randomizer = new Randomizer();
    
    public void Init(int offset)
    {
        _randomizer.Init();
        _nextPosition = offset + _randomizer.GetRndOffset();
    }
    
    public bool Generate(int currentPosition, out int row)
    {
        if (currentPosition < _nextPosition)
        {
            row = 0;
            return false;
        }
        row = _randomizer.GetRndRow();
        _nextPosition += _randomizer.GetRndOffset();
        return true;
    }

    public void Restart(int offset)
    {
        _randomizer.Reset();
        _nextPosition = offset + _randomizer.GetRndOffset();
    }
    
    public void RepositionBy(int reposition)
    {
        _nextPosition -= reposition;
    }
}