﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class Randomizer
{
    public Vector2Int Spectrum = new Vector2Int(0, 10);
    public AnimationCurve Percentage = AnimationCurve.Linear(0, 1,1,1);
    
    private float[] _normalizePercent;
    private readonly int[] _rowPercent = new int[Const.CountRows];

    public void Init()
    {
        var list = new List<float>();
        float sum = 0;
        float dist = Spectrum.y - Spectrum.x;
        for (var i = Spectrum.x; i <= Spectrum.y; i++)
        {
            var step = (i - Spectrum.x) / dist;
            sum += Percentage.Evaluate(step);;
            list.Add(sum);
        }
        _normalizePercent = new float[list.Count];
        for (var i = 0; i < list.Count; i++)
        {
            _normalizePercent[i] = list[i] / sum;
        }

        var s = 100/ _rowPercent.Length;
        for (var i = 0; i < _rowPercent.Length; i++)
        {
            _rowPercent[i] = s;
        }
    }

    public void Reset()
    {
        var s = 100/ _rowPercent.Length;
        for (var i = 0; i < _rowPercent.Length; i++)
        {
            _rowPercent[i] = s;
        }
    }
    
    public int GetRndOffset()
    {
        var r = Random.Range(0, 1.0f);
        var i = 0;
        while (r > _normalizePercent[i])
        {
            i++;
        }
        return Spectrum.x + i;
    }

    public int GetRndRow()
    {
        var r = Random.Range(0, 100);
        var index = 0;
        const int demode = 10;
        for (var i = 0; i < _rowPercent.Length; i++)
        {
            r -= _rowPercent[i];
            if (r > 0) continue;
            index = i;
            _rowPercent[i] -= demode * _rowPercent.Length;
            break;
        }
        
        for (var i = 0; i < _rowPercent.Length; i++)
        {
            _rowPercent[i] += demode;
        }
        return -Const.CountRows/2 + index;
    }
    
    public void TestOffset()
    {
        var arr = new int[Spectrum.y - Spectrum.x + 1];
        for (var i = 0; i < 10000; i++)
        {
            arr[GetRndOffset() - Spectrum.x]++;
        }

        var min = int.MaxValue;
        var max = 0;
        foreach (var a in arr)
        {
            max = Mathf.Max(max, a);
            min = Mathf.Min(min, a);
        }
        Debug.Log((float)min/max);
    }
    
    public void TestRow()
    {
        var arr = new int[Const.CountRows];
        for (var i = 0; i < 10000; i++)
        {
            arr[GetRndRow() + Const.CountRows/2]++;
        }

        var min = int.MaxValue;
        var max = 0;
        foreach (var a in arr)
        {
            max = Mathf.Max(max, a);
            min = Mathf.Min(min, a);
        }
        Debug.Log((float)min/max);
    }
}